//adding sound
var sound = new Audio('../alarm.wav');
sound.loop = true;


//adding zero
function checkTime(time) {
	return (time < 10) ? "0" + time : time;	
}

//filling select with hours
function hoursSelect(){
	var select = document.getElementById('alarm_hrs');
	var hrs = 12

	for (i=1; i <= hrs; i++) {
		select.options[select.options.length] = new Option( i < 10 ? "0" + i : i, i);		
	}
}
hoursSelect();

//filling select with minutes
function minSelect(){
	var select = document.getElementById('alarm_mins');
	var min = 59;

	for (i=0; i <= min; i++) {
		select.options[select.options.length] = new Option(i < 10 ? "0" + i : i, i);
	}
}
minSelect();

//filling select with seconds
function secSelect(){
	var select = document.getElementById('alarm_secs');
	var sec = 59;

	for (i=0; i <= sec; i++) {
		select.options[select.options.length] = new Option(i < 10 ? "0" + i : i, i);
	}
}
secSelect();


function setAlarm() {

	var hours = document.getElementById('alarm_hrs');
	var minutes = document.getElementById('alarm_mins');
	var seconds = document.getElementById('alarm_secs');
	var am_pm = document.getElementById('am_pm');
    
	//getting selected time for alarm
    var selectedHour = hours.options[hours.selectedIndex].value;
    var selectedMin = minutes.options[minutes.selectedIndex].value;
    var selectedSec = seconds.options[seconds.selectedIndex].value;
    var selectedAP = am_pm.options[am_pm.selectedIndex].value;

    var alarmTime = checkTime(selectedHour) + ":" + checkTime(selectedMin) + ":" + checkTime(selectedSec) + selectedAP;

    document.getElementById('alarm_hrs').disabled = true;
	document.getElementById('alarm_mins').disabled = true;
	document.getElementById('alarm_secs').disabled = true;
	document.getElementById('am_pm').disabled = true;



//function for comparing the current time with alarm time and playing the alarm if there is a match

setInterval(function(){

	var date = new Date();
	
	var hours = (12 - (date.getHours()));
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	
	var ampm = (date.getHours()) < 12 ? 'AM' : 'PM';


	//converting to standard time
	if (hours < 0) {
		hours = hours * -1;
	} else if (hours == 00) {
		hours = 12;
	} else {
		hours = 12 - hours;
	}

	var currentTime = checkTime(hours) + ":" + checkTime(minutes) + ":" + checkTime(seconds) + "" + ampm;
	
   //comparing, if match playing the alarm 
	if (alarmTime == currentTime) {
		sound.play();
		}

},1000);
	
}

//function for stopping the alarm
function clearAlarm() {

	document.getElementById('alarm_hrs').disabled = false;
	document.getElementById('alarm_mins').disabled = false;
	document.getElementById('alarm_secs').disabled = false;
	document.getElementById('am_pm').disabled = false;
	sound.pause();
}


