// function for getting digital time in both formats

function digitalTime() {
    var today = new Date();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    
    //24hour format
    document.getElementById('digital_24Hour').innerHTML =
    checkTime(hours) + ":" + checkTime(minutes) + ":" + checkTime(seconds)

    var am_pm = "AM" 

    if (hours > 12) { 
        hours -= 12; 
        am_pm = "PM"; 
    } 
    if (hours == 0) { 
        hours = 12; 
        am_pm = "AM"; 
    } 

    //12hour Format
    document.getElementById('digital_12Hour').innerHTML =
    checkTime(hours) + ":" + checkTime(minutes) + ":" + checkTime(seconds) + " " + am_pm;

    var t = setTimeout(digitalTime, 1000);

}

//adding zero
  function checkTime(i) {
    if (i < 10) {i = "0" + i}; 
    return i;
  }

//toggling between formats by using display property
  function changeFormat(){
    if(document.getElementById('digital_24Hour').style.display === "block"){
      document.getElementById('digital_24Hour').style.display = "none"
      document.getElementById('digital_12Hour').style.display = "block"
    }else if(document.getElementById('digital_12Hour').style.display === "block"){
      document.getElementById('digital_12Hour').style.display = "none"
      document.getElementById('digital_24Hour').style.display = "block"
    }
  }

// function for analog time using rotate property every second

  const hour = document.getElementById("hour-hand");
  const minute = document.getElementById("minutes-hand");
  const second = document.getElementById("seconds-hand");

  function analogTime() {
      const now = new Date();


      const seconds = now.getSeconds();
      const minutes = now.getMinutes();
      var hours = now.getHours();
      if(hours > 12){
          hours = hours - 12
      }
    
      //angles calculations
      const secondsAngle = ((seconds/60) * 360) + 90;
      const minutesAngle = (minutes/5) * 30 + 90;
      const hoursAngle =  30*(hours + (minutes/60)) + 90;
      
      //using rotate property
      second.style.transform = `rotate(${secondsAngle}deg)`
      minute.style.transform = `rotate(${minutesAngle}deg)`
      hour.style.transform = `rotate(${hoursAngle}deg)`

      var t = setTimeout(analogTime,1000);

  }

//function for toggling between analog and digital clock using display property
function changeClock(){
  if(document.getElementById('digital_clock').style.display === "block"){
    document.getElementById('digital_clock').style.display = "none"
    analogTime();
    document.getElementById('analog_clock').style.display = "block"
  }else if(document.getElementById('analog_clock').style.display === "block"){
    document.getElementById('analog_clock').style.display = "none"
    document.getElementById('digital_clock').style.display = "block"
  }
}